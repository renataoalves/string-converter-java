package view;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import controller.Controller;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class View extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private JLabel lblSeuTexto = new JLabel("Texto Inicial"), lblSuaString = new JLabel("String Final");
	private JTextArea txtStringInicial = new JTextArea(), txtStringFinal = new JTextArea();
	private JButton btnCleanAllLine = new JButton("Limpar Quebra de Linhas");
	private JScrollPane scrollTxtAreaTexto = new JScrollPane(txtStringInicial), scrollTxtAreaString = new JScrollPane();
	private final JMenuBar menuBar = new JMenuBar();
	private final JMenu mnArquivo = new JMenu("Arquivo");
	private final JMenuItem mntmSair = new JMenuItem("Sair");
	private final JMenu mnEditar = new JMenu("Editar");
	private final JMenuItem mntmCaixaAltaFinal = new JMenuItem("CAIXA ALTA");
	private final JMenuItem mntmCaixaBaixaFinal = new JMenuItem("caixa baixa");
	private final JMenuItem mntmCaixaAltaInicial = new JMenuItem("CAIXA ALTA");
	private final JMenuItem mntmCaixaBaixaInicial = new JMenuItem("caixa baixa");
	private final JMenu mnStringInicial = new JMenu("String Inicial");
	private final JMenu mnStringFinal = new JMenu("String Final");
	
	public View (){
		getContentPane().setLayout(null);
		
		lblSeuTexto.setBounds(10, 11, 94, 14);
		getContentPane().add(lblSeuTexto);
		
		lblSuaString.setBounds(10, 250, 94, 14);
		getContentPane().add(lblSuaString);
		
		scrollTxtAreaTexto.setBounds(10, 30, 724, 200);
		scrollTxtAreaTexto.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		getContentPane().add(scrollTxtAreaTexto);
		
		scrollTxtAreaString.setBounds(10, 275, 724, 200);
		scrollTxtAreaString.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
		getContentPane().add(scrollTxtAreaString);
		
		scrollTxtAreaString.setViewportView(txtStringFinal);
		
		txtStringFinal.setEditable(false);
		btnCleanAllLine.setToolTipText("Limpa mais de 1 Quebra de Linha");
		
		btnCleanAllLine.setBounds(529, 241, 205, 23);
		btnCleanAllLine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtStringFinal.setText(Controller.getCleanTwoMoreEmptyLine(txtStringInicial.getText()));
			}
		});
		getContentPane().add(btnCleanAllLine);
		
		JButton btnTransform = new JButton("Transformar");
		btnTransform.setToolTipText("Converte texto em String concatenada");
		btnTransform.setBounds(390, 241, 129, 23);
		btnTransform.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtStringFinal.setText(Controller.getTextFormatted(txtStringInicial.getText()));
			}
		});
		
		getContentPane().add(btnTransform);
		
		mntmCaixaAltaInicial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtStringInicial.setText(txtStringInicial.getText().toUpperCase());
			}
		});
		
		mntmCaixaBaixaInicial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtStringInicial.setText(txtStringInicial.getText().toLowerCase());
			}
		});
		
		mntmCaixaAltaFinal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtStringFinal.setText(txtStringFinal.getText().toUpperCase());
			}
		});
		
		mntmCaixaBaixaFinal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				txtStringFinal.setText(txtStringFinal.getText().toLowerCase());
			}
		});
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Conversor de Texto para Sting Java");
		setSize(750, 535);
		setLocationRelativeTo(null);
		
		setJMenuBar(menuBar);
		
		menuBar.add(mnArquivo);
		
		mnArquivo.add(mntmSair);
		
		menuBar.add(mnEditar);
		
		mnEditar.add(mnStringInicial);
		mnStringInicial.add(mntmCaixaAltaInicial);
		mnStringInicial.add(mntmCaixaBaixaInicial);
		
		mnEditar.add(mnStringFinal);
		mnStringFinal.add(mntmCaixaAltaFinal);
		mnStringFinal.add(mntmCaixaBaixaFinal);
		setVisible(true);
		setExtendedState(JFrame.MAXIMIZED_BOTH);
	}
}