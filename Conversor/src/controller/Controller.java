package controller;

import java.util.ArrayList;

public class Controller {
	
	private static String [] getRows(String string){
		return string.split("\n");
	}
	
	private static String rightTrim(String string){
		return string.replaceAll("\\s+$", "");
	}
	
	public static String getCleanTwoMoreEmptyLine(String string){
		
		String[] rows = getRows(string);
		
		String text = "";
		
		boolean isEmptyLine = false;
		
		for(int x=0; x<rows.length; x++){
			
			if(rows[x].trim().equals("")){
				isEmptyLine = true;
			}
			
			if(!rows[x].trim().equals("")){
				if(isEmptyLine){
					isEmptyLine = false;
					text += "\n";
				}
				
				text += rows[x]+"\n";
			}
			
		}
		
		return text;
	}
	
	public static String getTextFormatted(final String stringInitial) {

		String[] textFormatted = getRows(stringInitial);
		
		ArrayList<String> rows = new ArrayList<String>();
		
		for(int x=0; x<textFormatted.length; x++){
			if(x == textFormatted.length-1)
				rows.add("\" "+rightTrim(textFormatted[x])+" \"");
			else
				rows.add("\" "+rightTrim(textFormatted[x])+" \" +");
		}
		
		String stringFinal = "";
		
		for(int x=0; x<rows.size(); x++){
			stringFinal += rows.get(x)+"\n";
		}
		
		return stringFinal; 
	}
	
}
